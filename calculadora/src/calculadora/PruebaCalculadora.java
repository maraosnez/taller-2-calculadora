package calculadora;

import static org.junit.Assert.*;

import org.junit.Test;

public class PruebaCalculadora {

	@Test
	public void sumaPositivo() {
		int resultado = Calculadora.suma(2, 3);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void sumaNegativo() {
		int resultado = Calculadora.suma(-2, -3);
		int esperado = -5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void restaPositivo() {
		int resultado = Calculadora.resta(2, 3);
		int esperado = -1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void restaNegativo() {
		int resultado = Calculadora.resta(-2, -3);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void multiPositivo() {
		int resultado = Calculadora.multi(2, 3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void multiNegativo() {
		int resultado = Calculadora.multi(-2, -3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void multiCero() {
		int resultado = Calculadora.multi(0, 2);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void divPositivo() {
		int resultado = Calculadora.div(4, 2);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	@Test
	public void divNegativo() {
		int resultado = Calculadora.div(-4, -2);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	@Test
	public void divCero() {
		int resultado = Calculadora.div(0, 4);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
}
